import java.io.*;
import java.util.*;

public class FileArray {

	/**
	 * @param args
	 */
	ArrayList<String> saveData = new ArrayList<String>();

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		FileArray filearray = new FileArray();
		filearray.readFile();
		filearray.writeFile();
	}

	private void writeFile() {
		// TODO Auto-generated method stub
		System.out.println(" " + saveData);
		File file = new File("output.txt");
		try {
			file.createNewFile();
			FileWriter filewriter = new FileWriter(file);
			BufferedWriter bufferedwriter = new BufferedWriter(filewriter);
			for (int i = 0; i < saveData.size(); i++) {
				bufferedwriter.write(saveData.get(i));
			}
			bufferedwriter.flush();
			bufferedwriter.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void readFile() {
		try {
			FileReader filereader = new FileReader("text.txt");
			BufferedReader bufferedreader = new BufferedReader(filereader);
			while (true) {
				String data = bufferedreader.readLine();
				if (data.equals(null)) {
					break;
				}
				saveData.add(data);
			}
			bufferedreader.close();
		} catch (Exception e) {

		}
	}

}
