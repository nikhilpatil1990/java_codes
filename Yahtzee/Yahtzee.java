/*
 * File: Yahtzee.java
 * ------------------
 * This program will eventually play the Yahtzee game.
 */

import java.util.Arrays;

import acm.io.*;
import acm.program.*;
import acm.util.*;

public class Yahtzee extends GraphicsProgram implements YahtzeeConstants {

	/* Private instance variables */
	private int nPlayers;
	private String[] playerNames;
	private YahtzeeDisplay display;
	private RandomGenerator rgen = RandomGenerator.getInstance();
	private int[][] score;
	private int[] dice = new int[5];
	private int[] upperTotal;
	private int[] lowerTotal;
	private int[] Total;
	private int MAXPLAYERS = 4;
	private int ZERO = 0;
	private final int BONUS = 35;
	private final int MAXSCORE = 63;

	public static void main(String[] args) {
		new Yahtzee().start(args);
	}

	public void run() {
		IODialog dialog = getDialog();
		nPlayers = dialog.readInt("Enter number of players");
		playerNames = new String[nPlayers];
		while (nPlayers >= MAXPLAYERS || nPlayers <= ZERO) {
			dialog.showErrorMessage("Improper number of players");
			nPlayers = dialog.readInt("Enter number of players");
			playerNames = new String[nPlayers];
		}

		for (int i = 1; i <= nPlayers; i++) {
			playerNames[i - 1] = dialog.readLine("Enter name for player " + i);
		}
		display = new YahtzeeDisplay(getGCanvas(), playerNames);
		score = new int[nPlayers][N_SCORING_CATEGORIES];
		upperTotal = new int[nPlayers];
		lowerTotal = new int[nPlayers];
		Total = new int[nPlayers];
		playGame();

	}

	private void playGame() {
		int chances = 0;
		int players;
		while (chances < N_SCORING_CATEGORIES) {
			players = 0;
			while (players < nPlayers) {
				display.printMessage(playerNames[players] + "'s turn");
				display.waitForPlayerToClickRoll(players + 1);
				randomDice();
				secondChance();
				int category = display.waitForPlayerToSelectCategory();
				boolean correct = checkCategory(category);
				if (correct) {
					calScore(category, chances, players);
				} else {
					score[players][chances] = 0;
				}
				display.updateScorecard(category, players + 1,
						score[players][chances]);
				players++;
			}
			chances++;
		}
		calOutput();

	}

	private void calOutput() {
		int players = 0;
		while (players < nPlayers) {
			if (upperTotal[players] > MAXSCORE) {
				upperTotal[players] += BONUS;
			}
			display.updateScorecard(UPPER_SCORE, players + 1,
					upperTotal[players]);
			display.updateScorecard(LOWER_SCORE, players + 1,
					lowerTotal[players]);
			Total[players] = upperTotal[players] + lowerTotal[players];
			display.updateScorecard(TOTAL, players + 1, Total[players]);
			players++;
		}
	}

	private void secondChance() {
		int index = 0;
		while (index < 2) {
			display.printMessage("Select Roll dice to roll again");
			display.waitForPlayerToSelectDice();
			for (int i = 0; i < 5; i++) {
				if (display.isDieSelected(i)) {
					dice[i] = rgen.nextInt(1, 6);
				}
			}
			display.displayDice(dice);
			index++;
		}

	}

	private void randomDice() {
		for (int i = 0; i < N_DICE; i++) {
			dice[i] = rgen.nextInt(1, 6);
		}
		display.displayDice(dice);
	}

	private boolean checkCategory(int category) {
		switch (category) {
		case ONES:
		case TWOS:
		case THREES:
		case FOURS:
		case FIVES:
		case SIXES:
			for (int i = 0; i < N_DICE; i++) {
				if (dice[i] == category) {
					return true;
				}
			}
			break;
		case THREE_OF_A_KIND:
			int kind = verifyKind(category, THREES);
			if (kind == 3) {
				return true;
			}
			break;
		case FOUR_OF_A_KIND:
			int kind1 = verifyKind(category, FOURS);
			if (kind1 == 4) {
				return true;
			}
			break;
		case SMALL_STRAIGHT:
			int kind2 = verifyStr(category);
			if (kind2 == (dice.length - 1)) {
				return true;
			}
			break;
		case LARGE_STRAIGHT:
			int kind3 = verifyStr(category);
			if (kind3 == (dice.length)) {
				return true;
			}
			break;
		case FULL_HOUSE:
			boolean house = verifyFull(category);
			if (house) {
				return true;
			}
			break;
		case CHANCE:
			return true;
		case YAHTZEE:
			int kind4 = verfyYahtzee(category);
			if (kind4 == (dice.length)) {
				return true;
			}
			break;
		}
		return false;
	}

	private boolean verifyFull(int category) {
		int counter1 = 1;
		Arrays.sort(dice);
		for (int i = 0; i < N_DICE - 1; i++) {
			if (dice[i] == dice[i + 1]) {
				counter1++;
			}
		}
		if (counter1 == 4) {
			return true;

		}
		return false;
	}

	private int verfyYahtzee(int category) {
		int counter = 1;
		for (int i = 0; i < N_DICE - 1; i++) {
			if (dice[i] == dice[i + 1]) {
				counter++;
			}
		}
		return counter;
	}

	private int verifyStr(int category) {
		Arrays.sort(dice);
		int counter = 1;
		for (int i = 0; i < N_DICE - 1; i++) {
			if (dice[i] == (dice[i + 1] - 1)) {
				counter++;
			}
		}
		return counter;
	}

	private int verifyKind(int category, int type) {
		int counter = 0;
		for (int j = 0; j < N_DICE; j++) {
			counter = 0;
			for (int i = 0; i < N_DICE; i++) {
				if (dice[j] == dice[i]) {
					counter++;
					if (counter == type)
						return counter;
				}

			}
		}
		return counter;
	}

	private void calScore(int category, int chances, int players) {
		switch (category) {
		case ONES:
			score[players][chances] = calTotal(ONES);
			upperTotal[players] += score[players][chances];
			break;
		case TWOS:
			score[players][chances] = calTotal(TWOS);
			upperTotal[players] += score[players][chances];
			break;
		case THREES:
			score[players][chances] = calTotal(THREES);
			upperTotal[players] += score[players][chances];
			break;
		case FOURS:
			score[players][chances] = calTotal(FOURS);
			upperTotal[players] += score[players][chances];
			break;
		case FIVES:
			score[players][chances] = calTotal(FIVES);
			upperTotal[players] += score[players][chances];
			break;
		case SIXES:
			score[players][chances] = calTotal(SIXES);
			upperTotal[players] += score[players][chances];
			break;
		case THREE_OF_A_KIND:
			score[players][chances] = calKind();
			lowerTotal[players] += score[players][chances];
			break;
		case FOUR_OF_A_KIND:
			score[players][chances] = calKind();
			lowerTotal[players] += score[players][chances];
			break;
		case FULL_HOUSE:
			score[players][chances] = 25;
			lowerTotal[players] += score[players][chances];
			break;
		case SMALL_STRAIGHT:
			score[players][chances] = 30;
			lowerTotal[players] += score[players][chances];
			break;
		case LARGE_STRAIGHT:
			score[players][chances] = 40;
			lowerTotal[players] += score[players][chances];
			break;
		case YAHTZEE:
			score[players][chances] = 50;
			lowerTotal[players] += score[players][chances];
			break;
		case CHANCE:
			score[players][chances] = calKind();
			lowerTotal[players] += score[players][chances];
			break;
		}
	}

	private int calTotal(int number) {
		int tempScore = 0;
		for (int i = 0; i < N_DICE; i++) {
			if (dice[i] == number)
				tempScore += number;
		}
		return (tempScore);
	}

	private int calKind() {
		int tempScore = 0;
		for (int i = 0; i < N_DICE; i++) {
			tempScore += dice[i];
		}
		return (tempScore);
	}

}
