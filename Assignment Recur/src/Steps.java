import java.util.Scanner;

public class Steps {
	private int possibilities = 0;

	public int countWays(int numStairs) {	
		if ( numStairs == 0)
		{
			return possibilities++;
		}
		else
		{
			if ( numStairs >= 2)
			{
				countWays(numStairs - 2);
				
			}
			countWays(numStairs - 1);
		}
		return possibilities;
	}

	public static void main(String[] args) {
		int numStairs;
		Steps s = new Steps();
		Scanner in = new Scanner(System.in);
		System.out.println("Enter the stairs");
		numStairs = in.nextInt();
		int output = s.countWays(numStairs);
		System.out.println("Number of stairs : " + output);
	}
}
