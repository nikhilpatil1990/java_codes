import java.util.Scanner;

public class Permutation {
	
	public static void main(String args[]) {
		int start = 0;
		Permutation prem = new Permutation();
		String word = prem.getInput();
	//	prem.recSubsets("", word);
		System.out.println("  : " + prem.isPalindrome(word,start,word.length()-1));
	}

	private boolean isPalindrome(String word, int start, int finish) {
		
		while ((start) <=(finish))
		{
			if ( word.charAt(start) == word.charAt(finish))
			{
				start++;
				finish--;
			}
			else
				return false;
		}
		return true;
	}

	private boolean palindrome(String word) {
		// TODO Auto-generated method stub
		if (word.length() <= 1)
		{
			return true;
		}
		else
		{
			if (word.charAt(0) == word.charAt(word.length() - 1))
			{
				return palindrome(word.substring(1, word.length()-1));
			}
			else
				return false;
		}
	}

	private void recSubsets(String sofar, String rest) {
		if (rest.equals("")) {
			System.out.println(sofar);
		} else {
			recSubsets(sofar + rest.charAt(0), rest.substring(1));
			recSubsets(sofar, rest.substring(1));
		}
	}

	private String getInput() {
		Scanner input = new Scanner(System.in);
		return (input.nextLine());
	}
}
