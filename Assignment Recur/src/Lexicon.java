import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

/**
 * This class reads all words of the English language from a file (lexicon.txt)
 * and puts them into a HashSet. You can access the HashSet via the getLexicon()
 * method. See the main() method for an example usage of the class.
 * 
 * The word list comes from Julie Zelenski's lexicon.cpp class.
 * 
 * @author ralph@lano.de
 */
public class Lexicon {
	private Set<String> lexicon;
	private Set<String> prefixes;

	/**
	 * This is for test purposes only and shows how to use this class.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		Lexicon l = new Lexicon("lexicon.txt");
		System.out.println(l.contains("ralph"));
		System.out.println(l.containsPrefix("ra"));
	}

	/**
	 * Constructor: reads words from a text file and puts it into a HashSet.
	 * 
	 * @param fileName
	 *            file containing the words, one word per line.
	 */
	public Lexicon(String fileName) {
		lexicon = new TreeSet<String>();
		readLexiconFromFile(fileName);
		prefixes = new TreeSet<String>();
		createAllPrefixes();
		System.out.println("hi");
	}

	/**
	 * This takes the lexicon and creates all possible prefixes out of it. Not
	 * very efficient, but easy to implement.
	 */
	private void createAllPrefixes() {
		Iterator<String> iter = lexicon.iterator();
		while (iter.hasNext()) {
			String word = (String) iter.next();
			for (int i = 0; i < word.length(); i++) {
				prefixes.add(word.substring(0, i + 1));
				// System.out.println(word.substring(0, i+1));
			}
		}
	}

	/**
	 * Accessor method to get access to the HashSet.
	 * 
	 * @return a HashSet of Strings containing all words of the English language
	 */
	public Set<String> getLexicon() {
		return lexicon;
	}

	/**
	 * 
	 * @param word
	 *            the word to look for in the HashSet
	 * @return true if the word is in the HashSet, otherwise false
	 */
	public boolean contains(String word) {
		return lexicon.contains(word);
	}

	/**
	 * Looks for a prefix in the HashSet.
	 * 
	 * @param pre
	 *            Prefix to look for in the HashSet.
	 * @return true if a word that starts with the prefix is found in the
	 *         HashSet.
	 */
	public boolean containsPrefix(String pre) {
		if (pre.length() == 0) {
			return true;
		}
		return prefixes.contains(pre);
	}

	/**
	 * This method does all the work: it goes through the complete file and
	 * reads one line at a time, putting the words into a HashSet.
	 * 
	 * @param fileName
	 *            file containing the words, one word per line.
	 */
	private void readLexiconFromFile(String fileName) {
		BufferedReader in = null;
		try {
			in = new BufferedReader(new FileReader(fileName));
			String word = "";
			while ((word = in.readLine()) != null) {
				lexicon.add(word);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				in.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

}
