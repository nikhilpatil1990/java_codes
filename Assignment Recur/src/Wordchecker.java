import java.util.*;


public class Wordchecker {
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Lexicon lex = new Lexicon("lexicon.txt");
		Scanner sn1 = new Scanner(System.in);
		String temp1 = sn1.next();
		if (lex.contains(temp1))
		{
			checkWord(temp1,lex,temp1.length());
		}
		else
		{
			System.out.println("Improper first word");
		}
	}

	private static void checkWord(String temp1, Lexicon lex, int i) {
		// TODO Auto-generated method stub
		String temp = null;
		int counter = 0;
		Scanner sn2 = new Scanner(System.in);
		temp = sn2.next();
		if ( temp.equals(null))
		{
			System.out.println("Null word");
		}
		if (lex.contains(temp) && temp.length() == i)
		{
			for ( i = 0 ; i < temp.length() ; i++ )
			{
				if (temp1.charAt(i) == temp.charAt(i))
				{
					counter++;
				}
			}
			if ( counter == temp.length()-1)
			{
				checkWord(temp,lex,temp.length());
			}
			else
			{
				System.out.println("Improper new word");
				checkWord(temp1,lex,temp1.length());
			}
		}
		else
		{
			System.out.println("No word exists");
		}
	}

}
