import java.util.ArrayList;
import java.util.Scanner;

public class cellPhone {
	private String inputvalue = "";
	private ArrayList<String> list;
	public void listCompletions(String digitSequence, Lexicon lex) {
		while (true) {
			list = new ArrayList<String>();
			String prefix = "";
			calPrefix(prefix, digitSequence + inputvalue, lex);
			for (int i = 0; i < list.size(); i++) {
				checkWord(list.get(i), lex);
			}
			System.out.println("Enter digit");
			Scanner sn1 = new Scanner(System.in);
			inputvalue += sn1.next();
			if (inputvalue.endsWith("*")) {
				break;
			}

		}
	}

	public void checkWord(String string, Lexicon lex) {
		if (lex.contains(string)) {
			System.out.println("Word is :" + string);
		}
	}

	public void calPrefix(String prefix, String rest, Lexicon lex) {
		if (rest.length() == 0) {
			if (lex.containsPrefix(prefix)) {
				list.add(prefix);
				System.out.println("prefix is :" + prefix);
			}
		} else {
			String str = digitCal(rest.charAt(0));
			for (int i = 0; i < str.length(); i++) {
				calPrefix(prefix + str.charAt(0), rest.substring(1), lex);
			}
		}
	}

	private String digitCal(char c) {
		switch (c) {
		case '0':
			return "0";
		case '1':
			return "1";
		case '2':
			return "a" + "b" + "c";
		case '3':
			return "d" + "e" + "f";
		case '4':
			return "g" + "h" + "i";
		case '5':
			return "j" + "k" + "l";
		case '6':
			return "m" + "n" + "o";
		case '7':
			return "p" + "q" + "r";
		case '8':
			return "s" + "t" + "u";
		case '9':
			return "v" + "w" + "x" + "y" + "z";

		}
		return null;
	}

	public static void main(String[] args) {
		Lexicon lex = new Lexicon("lexicon.txt");
		cellPhone cell = new cellPhone();
		String digitSequence = cell.getInput();
		cell.listCompletions(digitSequence, lex);
	}

	private String getInput() {
		System.out.println("Enter first digit");
		Scanner sn = new Scanner(System.in);
		return sn.next();
		
	}
}
