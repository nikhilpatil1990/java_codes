import java.io.*;
import java.util.*;


public class Binary {
	private ArrayList<Integer> data  = new ArrayList<Integer>();
	public static void main(String[] args) {
		Binary b = new Binary();
		b.readFile();
		b.output();
	}

	private void output() {
		Scanner sn1 = new Scanner(System.in);
		System.out.println("Enter value to be searched : ");
		int value = sn1.nextInt();
		boolean out = search(data,value,0,data.size());
		System.out.println("Output : " + out);
	}

	private void readFile() {
		try {
			BufferedReader file = new BufferedReader(new FileReader("data.txt"));
			while (true)
			{
				String read = file.readLine();
				if ( read.equals(null))
					break;
				data.add(Integer.parseInt(read));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	private boolean search(ArrayList<Integer> a, int value, int i, int j) {
	
	int mid = (i+j)/2;
	
	if ( i > j)
		return false;
	
	if ( a.get(mid) == value )
	{
		return true;
	}
	else
	{
		if (a.get(mid) > value)
		{
			return search(a,value,i,mid-1);
		}
		else
		{
			return search(a,value,mid+1,j);	
		}
	}
	}
}
