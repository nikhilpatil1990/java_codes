/*
 * File: Breakout.java
 * -------------------
 * Name: Nikhil Patil
 * 
 * This file will eventually implement the game of Breakout.
 */

import acm.graphics.*;
import acm.program.*;
import acm.util.*;

import java.applet.*;
import java.awt.*;
import java.awt.event.*;

public class BreakOut extends GraphicsProgram {

	/** Width and height of application window in pixels */
	public static final int APPLICATION_WIDTH = 400;
	public static final int APPLICATION_HEIGHT = 600;

	/** Dimensions of game board (usually the same) */
	private static final int WIDTH = APPLICATION_WIDTH;
	private static final int HEIGHT = APPLICATION_HEIGHT;

	/** Dimensions of the paddle */
	private static final int PADDLE_WIDTH = 60;
	private static final int PADDLE_HEIGHT = 10;

	/** Offset of the paddle up from the bottom */
	private static final int PADDLE_Y_OFFSET = 30;

	/** Number of bricks per row */
	private static final int NBRICKS_PER_ROW = 10;

	/** Number of rows of bricks */
	private static final int NBRICK_ROWS = 10;

	/** Separation between bricks */
	private static final int BRICK_SEP = 4;

	/** Width of a brick */
	private static final int BRICK_WIDTH = (WIDTH - (NBRICKS_PER_ROW - 1)
			* BRICK_SEP)
			/ NBRICKS_PER_ROW;

	/** Height of a brick */
	private static final int BRICK_HEIGHT = 8;

	/** Radius of the ball in pixels */
	private static final int BALL_RADIUS = 10;

	/** Offset of the top brick row from the top */
	private static final int BRICK_Y_OFFSET = 70;

	/** Number of turns */
	private static final int NTURNS = 3;

	private static final int DELAY = 10;

	private static final double Y_VEL = 3;

	double yvel = Y_VEL;
	double xvel = 0.0;
	boolean flag1 = true;
	/* Method: run() */
	/** Runs the Breakout program. */
	GRect paddle;
	GRect[][] brick = new GRect[NBRICK_ROWS][NBRICKS_PER_ROW];;
	RandomGenerator rgn = RandomGenerator.getInstance();
	GOval ball;
	GObject collObj;
	private int currTurn = 0;
	private int score = 0;
	private int numBrick = 0;

	public void run() {
		setup();
		while (NTURNS > currTurn && numBrick != 100) {
			bounce();
			pause(DELAY);
		}
		setupOut();

	}

	private void bounce() {

		if ((ball.getX() < (getWidth() - WIDTH)) || flag1 == true) {
			xvel = rgn.nextDouble(1.0, 4.0);
			if (flag1 == true) {
				pause(1000);
				flag1 = false;
			}
		}

		if ((ball.getX() + (BALL_RADIUS * 2.0)) > getWidth()) {
			xvel = rgn.nextDouble(1.0, 4.0);
			xvel = -xvel;
		}

		if ((ball.getY() + (BALL_RADIUS * 2.0)) > getHeight()) {
			remove(ball);
			flag1 = true;
			setupBall();
			currTurn++;
			pause(200);
		}

		if (ball.getY() < 0) {
			yvel = -yvel;
			if (xvel < 0) {
				xvel = rgn.nextDouble(1.0, 4.0);
				xvel = -xvel;
			}
		}

		ball.move(xvel, yvel);
		getCollidingObject();

	}

	private void getCollidingObject() {
		collObj = getElementAt(ball.getX(), ball.getY());
		if (collObj != null) {
			if (collObj == paddle) {
				yvel = -yvel * rgn.nextDouble(1.5, 2.0);
				bounce();
				if (score > 0) {
					score--;
				}
			} else {
				removeBrick();
			}
		} else {
			collObj = getElementAt((ball.getX() + 2 * BALL_RADIUS), ball.getY());
			if (collObj != null && collObj != paddle) {
				removeBrick();

			} else {
				collObj = getElementAt(ball.getX(),
						(ball.getY() + 2 * BALL_RADIUS));
				if (collObj != null && collObj != paddle) {
					removeBrick();
				} else {
					collObj = getElementAt(ball.getX() + 2 * BALL_RADIUS,
							(ball.getY() + 2 * BALL_RADIUS));
					if (collObj != null && collObj != paddle) {
						removeBrick();
					}
				}
			}
		}
	}

	private void removeBrick() {
		numBrick++;
		if (yvel < 0) {
			yvel = -Y_VEL;
		} else {
			yvel = Y_VEL;
		}
		yvel = -yvel;
		remove(collObj);
		AudioClip hit = MediaTools.loadAudioClip("bounce.au");
		hit.play();
		score += 10;
	}

	public void mouseMoved(MouseEvent e) {
		if ((e.getX() + (BRICK_WIDTH)) <= getWidth()) {
			paddle.setLocation(e.getX(), (getHeight() - PADDLE_Y_OFFSET));

		}

	}

	private void setup() {
		int brickWth;
		int brickHt = BRICK_Y_OFFSET;
		for (int j = 0; j < NBRICK_ROWS; j++) {
			brickWth = 0;
			for (int i = 0; i < NBRICKS_PER_ROW; i++) {
				brick[j][i] = new GRect(brickWth, brickHt, BRICK_WIDTH,
						BRICK_HEIGHT);
				add(brick[j][i]);
				brickWth = (BRICK_SEP * (i + 1)) + (BRICK_WIDTH * (i + 1));
				brick[j][i].setFilled(true);
				switch (j) {
				case 0:
				case 1:
				case 2:
				case 3:
					brick[j][i].setColor(Color.RED);
					break;
				case 4:
				case 5:
					brick[j][i].setColor(Color.ORANGE);
					break;
				case 6:
				case 7:
					brick[j][i].setColor(Color.GREEN);
					break;
				case 8:
				case 9:
					brick[j][i].setColor(Color.CYAN);
					break;

				}
			}
			brickHt += ((BRICK_SEP) + (BRICK_HEIGHT));
		}

		paddle = new GRect(getWidth() / 2.0, (getHeight() - PADDLE_Y_OFFSET),
				PADDLE_WIDTH, PADDLE_HEIGHT);
		add(paddle);
		paddle.setFilled(true);
		paddle.setFillColor(Color.BLACK);
		setupBall();
		addMouseListeners();
		addMouseListeners();
	}

	private void setupBall() {
		ball = new GOval(getWidth() / 2.0, getHeight() / 2.0, BALL_RADIUS,
				BALL_RADIUS);
		add(ball);
		ball.setFilled(true);
		ball.setFillColor(Color.darkGray);
	}

	private void setupOut() {
		remove(ball);
		for (int j = 0; j < NBRICK_ROWS; j++) {
			for (int i = 0; i < NBRICKS_PER_ROW; i++) {
				remove(brick[j][i]);
			}
		}
		remove(paddle);
		ball = null;
		GLabel output = new GLabel("Your score is :" + Integer.toString(score),
				(getWidth() / 2 - 40), getHeight() / 2);
		add(output);
		output.setColor(Color.BLACK);
		if (numBrick == 100) {
			GImage img = new GImage(
					"/Hof data/eclipse_stanford_jre/jre/bin/winner-win.jpg",
					((getWidth() / 2) - 100), 0);
			img.setSize(200, 200);
			add(img);
		} else {
			GImage img = new GImage(
					"/Hof data/eclipse_stanford_jre/jre/bin/lose-lose.jpg",
					((getWidth() / 2) - 100), 0);
			img.setSize(200, 200);
			add(img);
		}
	}
}