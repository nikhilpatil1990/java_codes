/*
 * File: NameSurfer.java
 * ---------------------
 * When it is finished, this program will implements the viewer for
 * the baby-name database described in the assignment handout.
 */
import acm.program.*;

import java.awt.BorderLayout;
import java.awt.event.*;
import java.io.*;

import javax.swing.*;

//public class NameSurfer extends Program implements NameSurferConstants {
public class NameSurfer extends Program implements NameSurferConstants {

	/* Method: init() */
	/**
	 * This method has the responsibility for reading in the data base and
	 * initializing the interactors at the bottom of the window.
	 */
	private static final long serialVersionUID = 1L;
	private JButton but;
	private JButton but1;
	private JTextField tf;
	private JLabel lbl;
	FileReader fr;
	NameSurferDataBase db;
	NameSurferGraph sg;
	String line;
	BufferedReader br;

	public void init() {
		lbl = new JLabel("Name");
		add(lbl, SOUTH);
		tf = new JTextField(20);
		tf.setEditable(true);
		add(tf, SOUTH);
		but = new JButton("Graphic");
		add(but, SOUTH);
		but1 = new JButton("Clear");
		add(but1, SOUTH);
		setSize(APPLICATION_WIDTH, APPLICATION_HEIGHT);
		sg = new NameSurferGraph();
		// sg.drawLine();
		add(sg);

		db = new NameSurferDataBase("names-data.txt");
		tf.addActionListener(this);
		addActionListeners();

	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == but) {
			// NameSurferDataBase db = new NameSurferDataBase("names-data.txt");
			String entry = (db.findEntry(tf.getText()));
			sg.addEntry(entry);
			sg.update();
			println(entry);
		}

		if (e.getSource() == but1) {
			sg.clear();
 		}
	}

	/* Method: actionPerformed(e) */
	/**
	 * This class is responsible for detecting when the buttons are clicked, so
	 * you will have to define a method to respond to button actions.
	 */
}
