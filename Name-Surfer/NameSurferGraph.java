/*
 * File: NameSurferGraph.java
 * ---------------------------
 * This class represents the canvas on which the graph of
 * names is drawn. This class is responsible for updating
 * (redrawing) the graphs whenever the list of entries changes or the window is resized.
 */

import acm.graphics.*;
import acm.program.GraphicsProgram;

import java.awt.event.*;
import java.util.*;
import java.awt.*;

public class NameSurferGraph extends GCanvas implements NameSurferConstants,
		ComponentListener {

	int[] result;
	String firstName = null;

	/**
	 * Creates a new NameSurferGraph object that displays the data.
	 */
	public NameSurferGraph() {
		addComponentListener(this);
	}

	public void drawBorder() {
		int x = getWidth();
		int y = getHeight();
		GLine downLine = new GLine(0, (y - GRAPH_MARGIN_SIZE), x,
				(y - GRAPH_MARGIN_SIZE));
		GLine upLine = new GLine(0, GRAPH_MARGIN_SIZE, x, GRAPH_MARGIN_SIZE);
		add(downLine);
		add(upLine);
	}

	/**
	 * Clears the list of name surfer entries stored inside this class.
	 */
	public void clear() {
		// You fill this in //
		removeAll();
		update();

	}

	/* Method: addEntry(entry) */
	/**
	 * Adds a new NameSurferEntry to the list of entries on the display. Note
	 * that this method does not actually draw the graph, but simply stores the
	 * entry; the graph is drawn by calling update.
	 */
	public void addEntry(String entry) {
		int j = 0;
		String[] tokens = entry.split(" ");
		result = new int[NDECADES];
		firstName = tokens[0];
		for (int i = 1; i <= NDECADES; i++) {

			try {
				result[j] = Integer.parseInt(tokens[i]);
			} catch (Exception e) {
				e.printStackTrace();
			}
			j++;
		}
	}

	/**
	 * Updates the display image by deleting all the graphical objects from the
	 * canvas and then reassembling the display according to the list of
	 * entries. Your application must call update after calling either clear or
	 * addEntry; update is also called whenever the size of the canvas changes.
	 */
	public void update() {
		removeAll();
		drawBorder();
		drawVertical();
		drawGraph();
	}

	public void drawGraph() {
		// TODO Auto-generated method stub
		if (firstName != null) {
			drawLine();
		}
	}

	public void drawLine() {
		// TODO Auto-generated method stub
		double properHt = (MAX_RANK / (getHeight() - (2 * GRAPH_MARGIN_SIZE)));
		int vx = 0;
		int vx2 = 0;
		double rank = 0;
		double rank2 = 0;
		int border = (getHeight() - GRAPH_MARGIN_SIZE);
		Color rand = randColor();
		for (int i = 0; i < result.length - 1; i++) {
			rank = result[i] / properHt;
			rank2 = result[i + 1] / properHt;
			vx2 = vx + (APPLICATION_WIDTH / NDECADES);
			if (rank != 0 && rank2 != 0) {
				createGraph(vx, rank + GRAPH_MARGIN_SIZE, vx2, rank2
						+ GRAPH_MARGIN_SIZE, i, rand);
			} else {
				if (rank != 0 && rank2 == 0) {
					createGraph(vx, rank + GRAPH_MARGIN_SIZE, vx2,
							(getHeight() - GRAPH_MARGIN_SIZE), i, rand);
				} else if (rank2 != 0 && rank == 0) {
					createGraph(vx, (getHeight() - GRAPH_MARGIN_SIZE), vx2,
							rank2 + GRAPH_MARGIN_SIZE, i, rand);
				} else if (rank2 == 0 && rank == 0) {
					createGraph(vx, border, vx2, border, i, rand);
				}
			}
			dispLast(i, result, rank2, vx2, border, rand);
			vx += (APPLICATION_WIDTH / NDECADES);
			rank = 0;
			rank2 = 0;
		}
	}

	public void createGraph(int vx, double rank, int vx2, double rank2, int i,
			Color rand) {
		// TODO Auto-generated method stub
		GLine line = new GLine(vx, (rank), vx2, (rank2));
		add(line);
		line.setColor(rand);
		GLabel name = new GLabel(firstName + result[i],
				(vx + GRAPH_MARGIN_SIZE), rank);
		add(name);
		name.setColor(rand);
	}

	private void dispLast(int i, int[] result, double rank2, int vx2,
			int border, Color rand) {
		// TODO Auto-generated method stub

		if (i == result.length - 2) {
			if (rank2 != 0) {
				GLabel name = new GLabel(firstName + result[i + 1], vx2, rank2);
				add(name);
				name.setColor(rand);
			} else {
				GLabel name = new GLabel(firstName + result[i + 1], vx2, border);
				add(name);
				name.setColor(rand);
			}

		}

	}

	public Color randColor() {
		// TODO Auto-generated method stub
		Random rand = new Random();
		float r = rand.nextFloat();
		float g = rand.nextFloat();
		float b = rand.nextFloat();
		Color randomColor = new Color(r, g, b);
		return randomColor;
	}

	public void drawVertical() {
		// TODO Auto-generated method stub
		int vx = 0;
		int decade = START_DECADE;
		for (int i = 0; i < NDECADES; i++) {
			GLine line = new GLine(vx, 0, vx, getHeight());
			GLabel label = new GLabel(String.valueOf(decade), vx, (getHeight()));
			add(label);
			add(line);
			decade += 10;
			vx += (getWidth() / NDECADES);
		}
	}

	/* Implementation of the ComponentListener interface */
	public void componentHidden(ComponentEvent e) {
	}

	public void componentMoved(ComponentEvent e) {
	}

	public void componentResized(ComponentEvent e) {
		update();
	}

	public void componentShown(ComponentEvent e) {
	}
}
