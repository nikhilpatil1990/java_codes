import java.io.*;
import java.util.HashMap;
import java.util.Map;

/*
 * File: NameSurferDataBase.java
 * -----------------------------
 * This class keeps track of the complete database of names.
 * The constructor reads in the database from a file, and
 * the only public method makes it possible to look up a
 * name and get back the corresponding NameSurferEntry.
 * Names are matched independent of case, so that "Eric"
 * and "ERIC" are the same names.
 */
public class NameSurferDataBase implements NameSurferConstants {
	private HashMap<String, String> store = new HashMap<String, String>();

	/* Constructor: NameSurferDataBase(filename) */
	/**
	 * Creates a new NameSurferDataBase and initializes it using the data in the
	 * specified file. The constructor throws an error exception if the
	 * requested file does not exist or if an error occurs as the file is being
	 * read.
	 */
	public NameSurferDataBase(String filename) {
		try {
			FileReader fr = new FileReader(filename);
			BufferedReader rd = new BufferedReader(fr);
			while (true) {
				String line = rd.readLine();
				if (line == null)
					break;
				NameSurferEntry entry = new NameSurferEntry(line);
				store.put(entry.getName(), entry.toString());
			}
			rd.close();
		} catch (IOException ex) {
			throw new Error(ex);
		}
	}

	/* Method: findEntry(name) */
	/**
	 * Returns the NameSurferEntry associated with this name, if one exists. If
	 * the name does not appear in the database, this method returns null.
	 */
	public String findEntry(String name) {
		return store.get(name);
	}

}