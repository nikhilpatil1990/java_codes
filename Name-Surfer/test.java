import acm.graphics.GLine;
import acm.program.*;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.*;
import java.io.*;

import javax.swing.*;

public class test extends GraphicsProgram
{
	private JButton but;
	private JButton but1;
	private JButton but2;
	private JButton but3;
	private GLine line;
	private GLine line1;
	private GLine line2;
	public final int PIXAL = 20;
	public double x = getWidth()/2;
	public double y = getHeight()/2;
	public void init()
	{
		but  = new JButton("North");
		add(but,SOUTH);
		but1 = new JButton("South");
		add(but1,SOUTH);
		but2 = new JButton("East");
		add(but2,SOUTH);
		but3 = new JButton("West");
		add(but3,SOUTH);
		setSize(500,500);
		x = getWidth()/2;
		y = getHeight()/2;
		addActionListeners();
		update();
		
	}
	private void update() {
				line = new GLine((x-5),(y-5),(x+5),(y+5));
				add(line);
				line1 = new GLine((x+5),(y-5),(x-5),(y+5));
				add(line1);
	}
	public void actionPerformed(ActionEvent e)
	{
		if (e.getSource() == but)
		{
			addLine(x,y,x,y-PIXAL);
			y -= PIXAL;
			
			update();
		}
		if (e.getSource() == but1)
		{
			addLine(x,y,x,y+PIXAL);
			y += PIXAL;
			update();
		}
		if (e.getSource() == but2)
		{
			addLine(x,y,x+PIXAL,y);
			x += PIXAL;
			update();
		}
		if (e.getSource() == but3)
		{
			addLine(x,y,x-PIXAL,y);
			x -= PIXAL;
			update();
		}
	}
	private void addLine(double x2, double y2, double x3, double d) {
		line2 = new GLine(x2,y2,x3,d);
		add(line2);
		line2.setColor(Color.RED);
		
	}
	
	}

